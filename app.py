import json
from PythonProject import function
from flask import Flask

app=Flask(__name__)

@app.route('/PythonFunction')
def usingFunction():
    return {"Function return": json.dumps(function(23)) }

if __name__=="__main__":
    app.run(debug=True)




